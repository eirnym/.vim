" vim: set sw=4 sts=4 et fdm=marker:
" SECTION: On-load settings {{{1
"============================================================
" Security settings {{{2
"============================================================
let skip_defaults_vim=1  " prevent to include defaults.vim
augroup vimStartup | au! | augroup END
augroup fedora | au! | augroup END
set nocompatible
set secure
set nomodeline

" Local Python settings {{{2
"============================================================
" disable Python 2 as soon as possible
if has('python3')
  py3 1+2
  pyx 1+2
endif

" SECTION: Plugin load list {{{1
" VimPlug plugins {{{2
"============================================================
function! BuildYCM(info)
  " info is a dictionary with 3 fields
  " - name:   name of the plugin
  " - status: 'installed', 'updated', or 'unchanged'
  " - force:  set on PlugInstall! or PlugUpdate!
  if a:info.status == 'installed' || a:info.force
    !python3 ./install.py --clang-completer
  endif
endfunction

function! s:InstallVimPlug()
    if empty(glob('~/.vim/autoload/plug.vim'))
      silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
      autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
    endif
endfunction

call s:InstallVimPlug()
call plug#begin('~/.vim/plugged')
" ----- Misc ----- {{{3
"============================================================
" Save edits to local git repos
Plug 'eirnym/vim-autogit'

Plug 'ctrlpvim/ctrlp.vim'

" OceanicNext color scheme
Plug 'mhartington/oceanic-next'
" colors and color schemes
Plug 'flazz/vim-colorschemes'

" Integration with git
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
" ----- UI ----- {{{3
"============================================================
"Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
" Nice modeline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'


" ----- Editor support ----- {{{3
"============================================================
" Colored brackets
Plug 'luochen1990/rainbow'
" Automatically insert pairs
"Plug 'jiangmiao/auto-pairs'
Plug 'LunarWatcher/auto-pairs'
" Edit surround tags
Plug 'tpope/vim-surround'
" Whitespace remover (for syntax highlighting)
Plug 'ntpeters/vim-better-whitespace'
" HTML/CSS speed-up
Plug 'mattn/emmet-vim', { 'for': [ 'html', 'css' ]}
" VimScript tests
Plug 'junegunn/vader.vim', { 'for': 'vim' }
" ASCII tables
Plug 'dhruvasagar/vim-table-mode', { 'on': 'TableModeEnable' }

" ----- Syntax ------ {{{3
"============================================================
" PG Sql syntax
Plug 'lifepillar/pgsql.vim', { 'for': 'sql' }


if has('gui_running') || $TERM == "xterm-kitty"
    Plug 'catppuccin/vim', { 'as': 'catppuccin' }
endif
" Pyrhon-based {{{3
"============================================================
" technically those plugins will work with Python2, but I prefer use
" Python3-only environments

if has('python3')  " {{{4
"============================================================
  " Python indentation
  Plug 'Vimjas/vim-python-pep8-indent', { 'for': 'python' }
  " Fuzzier Search
  Plug 'Yggdroot/LeaderF', { 'do': './install.sh' }
  " YouCompleteMe
  Plug 'Valloric/YouCompleteMe', { 'do': function('BuildYCM') }
  " Black formatter
  Plug 'psf/black', { 'for': 'python' }
  " Minimap on a side
  " Plug 'severin-lemaignan/vim-minimap'
  "Plug 'tweekmonster/impsort.vim'
  " Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}
  "Plug 'jaxbot/semantic-highlight.vim'
endif


"Plug 'scrooloose/nerdcommenter'
"Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin' }
"Plug 'junegunn/fzf.vim'

" Javascript-related plugins   " {{{3
"============================================================
"Plug 'prettier/vim-prettier', {
"  \ 'do': 'npm install',
"  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue'] }
" vim-jsx-improve includes vim-javascript
"Plug 'pangloss/vim-javascript', { 'for': [ 'javascript', 'jsx' ]}
"Plug 'neoclide/vim-jsx-improve', { 'for': [ 'javascript', 'jsx' ] }
"Plug 'leafgarland/typescript-vim', { 'for': [ 'typescript', 'tsx' ] }
"Plug 'w0rp/ale'
"Plug 'styled-components/vim-styled-components', { 'branch': 'main' }
"Plug 'HerringtonDarkholme/yats.vim/'
"Plug 'othree/html5.vim'
"Plug 'othree/yajs.vim'
"Plug 'ryanoasis/vim-devicons'
"
"
"sheerun/vim-polyglot
"

Plug 'dstein64/vim-startuptime'
call plug#end()
" Vim 8 packages {{{2

if has('python3')
    packadd! editorconfig
endif

packadd! matchit

" SECTION: Vim global settings {{{1
"============================================================
"helptags  " regenerate help tags

" Vim UI settings {{{2
"============================================================
if has('gui_running')
    colo catppuccin_macchiato
    if has('gui_macvim')
        set macligatures
        set antialias
        set smoothscroll
    endif
    " hi String ctermfg=114 guifg=#99c794

    set guifont=Fira\ Code\ Light:h12
        " Font test
        " ≄
        " c == a != b  <-- ligature test
        " Some languages:
        " Привет гостям! <-- Russian test
        " Cześć  <-- Polish test
        " γεια  <-- Greek chars
        " 暗示するより明示するほうがいい。 <--Japanese chars
        "                                     (Beautiful is better than ugly)
        " *a **abc) ***abc
        " ++a a---
        " www != ^= ~= ~@
        " *ptr 9:45 0xff
        " <*> <|||>>>
        " <~> <~~ --> === =:=
        " <+> ?? #{  #_ #_( ?= ^=
        " PowerLine:
        "  ro=, ws=☲, lnr=☰, mlnr=, br=, nx=Ɇ, crypt=🔒
        "  ro=⊝, ws=☲, lnr=☰, mlnr=㏑, br=ᚠ, nx=Ɇ, crypt=🔒
        "     
        " Emoji: 😀😂🙃

else
    set background=dark
    set mouse=
    set t_ut=
        set termguicolors
    if $TERM == "xterm-kitty"
        colo catppuccin_macchiato
        set termguicolors
    else
        colo Chasing_Logic_mod
        set t_Co=256
        set notermguicolors
    endif
endi

" Vim options {{{2
"============================================================
"filetype off
syntax on
syntax sync fromstart
filetype plugin indent on

" Vim defaults {{{ 3
"============================================================

" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set wildmenu		" display completion matches in a status line

set ttyfast         " Indicates a fast terminal connection.
set ttimeout		" time out for key codes
set ttimeoutlen=100	" wait up to 100ms after Esc for special key

" Show @@@ in the last line if it is truncated.
set display=truncate

" Show a few lines of context around the cursor.  Note that this makes the
" text scroll if you mouse-click near the start or end of the window.
set scrolloff=5


" Do not recognize octal numbers for Ctrl-A and Ctrl-X, most users find it
" confusing.
set nrformats-=octal

" Don't use Ex mode, use Q for formatting.
" Revert with ":unmap Q".
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
" Revert with ":iunmap <C-U>".
inoremap <C-U> <C-G>u<C-U>

" I like highlighting strings inside C comments.
" Revert with ":unlet c_comment_strings".
let c_comment_strings=1

" Prevent that the langmap option applies to characters that result from a
" mapping.  If set (default), this may break plugins (but it's backward
" compatible).
set nolangremap

" Vim my defaults {{{ 3
"============================================================

set autoindent
set cindent
set completeopt-=preview
set esckeys
set expandtab
set nohlsearch
set noincsearch
set history=2000
set laststatus=2
set number
set shiftwidth=4
set smartindent
set smarttab
set tabpagemax=200
set tabstop=4
set nowrap
let g:netrw_dirhistmax=0
set signcolumn=yes
set nolangremap

command! DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis | wincmd p | diffthis

" Vim keybindings {{{2
"============================================================
" Some mappings
nmap <S-D-Left> gT
nmap <S-D-Right> gt
vmap <S-D-Left> gT
vmap <S-D-Right> gt
map! <S-D-Left> <ESC>gT
map! <S-D-Right> <ESC>gt
nmap <M-j> mz:m+<CR>`z
nmap <M-k> mz:m-2<CR>`z
nmap <D-j> mz:m+<CR>`z
nmap <D-k> mz:m-2<CR>`

" map Mac OS X Terminal.app default Home and End
map <ESC>[H <Home>
map <ESC>[F <End>
imap <ESC>[H <C-O><Home>
imap <ESC>[F <C-O><End>
cmap <ESC>[H <Home>
cmap <ESC>[F <End>


" SECTION: Global autocommands {{{2
"============================================================
let s:PythonColumnsSet = 0

fun! s:PythonSetup()
    let python_highlight_all = 1

    if has('gui_running')
        if (s:PythonColumnsSet)
            return
        endif
        let s:PythonColumnsSet = 1
        " line number (3 digits) + hidden color column at 89
        setlocal columns=92
    endif
endfun

fun! s:VimSetup()
    set sw=4 sts=4 et fdm=marker
    let g:strip_only_modified_lines=1
endfun

" FIXME: Place it into proper place in the file
fun! s:DiffSetup()
    let g:show_spaces_that_precede_tabs = 0
    let g:strip_whitelines_at_eof = 0
    let g:strip_only_modified_lines = 1
    let g:strip_whitespace_confirm = 0
endfun

augroup eirnymDefaults
    au!
    autocmd FileType python call s:PythonSetup()
    autocmd FileType vim call s:VimSetup()
    autocmd FileType gitcommit call s:DiffSetup()
augroup end

augroup macvimRedraw
    au!
    " autocmd FocusLost * redraw!
    " autocmd VimResized * redraw!
augroup end

augroup binary_files
    au!
    autocmd! BufReadPre,FileReadPre *.rpyc set bin
    autocmd! BufReadPre,FileReadPre *.rpymc set bin
    autocmd! BufReadPre,FileReadPre *.pyc set bin
    autocmd! BufReadPre,FileReadPre *.torrent set bin
augroup end
" SECTION: Plugin settings {{{1
"============================================================

" PostgreSQL syntax by default  {{{2
"============================================================
let g:sql_type_default = 'pgsql'
" let g:pgsql_backslash_quote = 1

" YouCompleteMe  {{{2
"============================================================
let g:ycm_autoclose_preview_window_after_completion = 1

fun! YcmBindings()
    nnoremap <buffer> <C-]> :YcmCompleter GoToDeclaration<CR>
    nnoremap <buffer> <C-k> :YcmCompleter GetDoc<CR>
    nnoremap <buffer> <C-P> :YcmCompleter GoToReferences<CR>
endfun
autocmd FileType python call YcmBindings()

" AutoGit  {{{2
"============================================================
if !exists("b:autogit_enabled") || b:autogit_enabled == "0"
    if exists("autogit#ToggleAutogit")
      call autogit#ToggleAutogit()
    endif
endif

" Rainbow brackets {{{2
"============================================================
let g:rainbow_active = 1
hi pythonRainbow_p0 gui=bold
hi pythonRainbow_p1 gui=bold
hi pythonRainbow_p2 gui=bold
hi pythonRainbow_p3 gui=bold
"syn match parenError ")"
"syn region matchingParens transparent start="(" end=")" contains=matchingParens
"hi def link parenError Error

" AirLine {{{2
"============================================================
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled = 0

" let g:airline#extensions#branch#enabled = 1

" EditorConfig {{{2
"============================================================
let g:EditorConfig_verbose = 0

" vim-markdown {{{2
"============================================================
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_fenced_languages = ['js=javascript']

" devicons {{{2
"============================================================
let g:webdevicons_enable_airline_tabline=1
let g:webdevicons_enable_airline_statusline=1
let g:webdevicons_enable_airline_statusline_fileformat_symbols=1

" TagBar  {{{2
"============================================================
"nnoremap <F8> :TagbarToggle<CR>
"let g:tagbar_autofocus = 1
"let g:tagbar_sort = 0 "tagbar shows tags in order of they created in file
"let g:tagbar_foldlevel = 0 "close tagbar folds by default

let g:oceanic_next_terminal_bold = 0

" CtrlP {{{2
"============================================================
let g:ctrlp_match_window='bottom,order:ttb'

" empty section {{{2
"============================================================
