let num = 255
while num >= 0
    let col = 255 - num
    exec 'hi col_'.num.' ctermbg='.num.' ctermfg=white cterm=bold'
    exec 'syn match col_'.num.' "ctermbg='.num.':....*$" containedIn=ALL'
    call append(0, 'ctermbg='.num.':.... Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat')
    call append(0, 'ctermbg='.num.':.... Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat')
    let num = num - 1
endwhile
