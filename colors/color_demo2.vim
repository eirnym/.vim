let num = 255
while num >= 0
    call append(0, 'ctermbg='.num.':....')
    let num = num - 1
endwhile
